package layers

import (
	"math"

	"gitlab.com/akita/dnn/tensor"
)

// ReluLayer implements a Rectified Linear Unit.
type ReluLayer struct {
	forwardInput []float64
}

func NewReluLayer() *ReluLayer {
	return &ReluLayer{}
}

func (r *ReluLayer) Randomize() {
	// This function is intentionally left blank
}

func (r *ReluLayer) Forward(
	inputTensor tensor.Tensor,
) tensor.Tensor {
	input := inputTensor.(*tensor.SimpleTensor)
	output := &tensor.SimpleTensor{}

	r.forwardInput = make([]float64, len(input.Vector()))
	copy(r.forwardInput, input.Vector())

	output.Init(make([]float64, len(input.Vector())),
		input.Size())
	for i, in := range input.Vector() {
		output.Vector()[i] = math.Max(0, in)
	}

	return output
}

func (r *ReluLayer) Backward(inputTensor tensor.Tensor) tensor.Tensor {
	input := inputTensor.(*tensor.SimpleTensor)
	output := &tensor.SimpleTensor{}

	output.Init(make([]float64, len(input.Vector())), input.Size())
	for i, in := range input.Vector() {
		if r.forwardInput[i] < 0 {
			output.Vector()[i] = 0
		} else {
			output.Vector()[i] = in
		}
	}

	return output
}

func (r ReluLayer) Parameters() tensor.Vector {
	return nil
}

func (r ReluLayer) Gradients() tensor.Vector {
	return nil
}
