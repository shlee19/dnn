package layers

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/dnn/tensor"
)

var _ = Describe("Relu Layer", func() {
	var (
		reluLayer *ReluLayer
		input     *tensor.SimpleTensor
	)

	BeforeEach(func() {
		input = &tensor.SimpleTensor{}
		reluLayer = NewReluLayer()
	})

	It("should forward", func() {
		input.Init([]float64{
			1, -1,
			2, 3,
		}, []int{2, 2})

		output := reluLayer.Forward(input)

		Expect(output.Size()).To(Equal([]int{2, 2}))
		Expect(output.Vector()).To(Equal([]float64{1, 0, 2, 3}))
		Expect(reluLayer.forwardInput).To(Equal(input.Vector()))
	})

	It("should backward", func() {
		input.Init([]float64{10, 20, 3, 4}, []int{2, 2})
		reluLayer.forwardInput = []float64{1, -1, 2, 3}

		output := reluLayer.Backward(input)

		Expect(output.Size()).To(Equal([]int{2, 2}))
		Expect(output.Vector()).To(Equal([]float64{10, 0, 3, 4}))
	})
})
