package layers

import (
	"math/rand"

	"gonum.org/v1/gonum/mat"

	"gonum.org/v1/gonum/blas"

	"gonum.org/v1/gonum/blas/blas64"

	"gitlab.com/akita/dnn/tensor"
)

// A FullyConnectedLayer implements a fully connected layer.
type FullyConnectedLayer struct {
	InputSize  int
	OutputSize int

	parameters      []float64
	weights         []float64
	bias            []float64
	gradients       []float64
	weightGradients []float64
	biasGradients   []float64
	forwardInput    []float64
}

// NewFullyConnectedLayers creates a fully connected layer.
func NewFullyConnectedLayer(inputSize, outputSize int) *FullyConnectedLayer {
	numWeight := inputSize * outputSize
	numBias := outputSize

	l := &FullyConnectedLayer{
		InputSize:  inputSize,
		OutputSize: outputSize,
		parameters: make([]float64, numWeight+numBias),
		gradients:  make([]float64, numWeight+numBias),
	}

	l.weights = l.parameters[:numWeight]
	l.bias = l.parameters[numWeight:]
	l.weightGradients = l.gradients[:numWeight]
	l.biasGradients = l.gradients[numWeight:]

	return l
}

func (l *FullyConnectedLayer) Randomize() {
	numWeight := l.InputSize * l.OutputSize
	for i := 0; i < numWeight; i++ {
		l.weights[i] = (rand.Float64() - 0.5) / float64(l.InputSize) * 2
	}

	numBias := l.OutputSize
	for i := 0; i < numBias; i++ {
		l.bias[i] = rand.Float64()*2 - 1
	}
}

func (l *FullyConnectedLayer) Forward(
	inputTensor tensor.Tensor,
) tensor.Tensor {
	input := inputTensor.(*tensor.SimpleTensor)
	output := &tensor.SimpleTensor{}

	l.saveForwardInput(input)

	outputData := l.forwardMatrixMultiplication(input)

	output.Init(outputData, []int{input.Size()[0], l.OutputSize})
	return output
}

func (l *FullyConnectedLayer) forwardMatrixMultiplication(
	input *tensor.SimpleTensor,
) []float64 {
	inputMatrix := mat.NewDense(input.Size()[0], input.Size()[1], input.Vector())
	weightMatrix := mat.NewDense(l.InputSize, l.OutputSize, l.weights)
	outputData := make([]float64, input.Size()[0]*l.OutputSize)
	for i := 0; i < input.Size()[0]; i++ {
		start := i * l.OutputSize
		end := start + l.OutputSize
		copy(outputData[start:end], l.bias)
	}
	outputMatrix := mat.NewDense(input.Size()[0], l.OutputSize, outputData)

	blas64.Gemm(blas.NoTrans, blas.NoTrans,
		1, inputMatrix.RawMatrix(), weightMatrix.RawMatrix(),
		1, outputMatrix.RawMatrix())
	return outputData
}

func (l *FullyConnectedLayer) Backward(
	inputTensor tensor.Tensor,
) tensor.Tensor {
	for i := range l.gradients {
		l.gradients[i] = 0
	}

	input := inputTensor.(*tensor.SimpleTensor)
	output := &tensor.SimpleTensor{}

	l.calculateWeightGradients(input)
	l.calculateBiasGradients(input)
	l.calculateInputGradients(input, output)

	return output
}

func (l *FullyConnectedLayer) calculateWeightGradients(input *tensor.SimpleTensor) {
	forwardMatrix := mat.NewDense(
		input.Size()[0], l.InputSize,
		l.forwardInput,
	)
	diffMatrix := mat.NewDense(
		input.Size()[0], input.Size()[1],
		input.Vector(),
	)
	weightGradMatrix := mat.NewDense(
		l.InputSize, l.OutputSize,
		l.weightGradients,
	)
	blas64.Gemm(blas.Trans, blas.NoTrans,
		1, forwardMatrix.RawMatrix(), diffMatrix.RawMatrix(),
		1, weightGradMatrix.RawMatrix())
}

func (l *FullyConnectedLayer) calculateBiasGradients(input *tensor.SimpleTensor) {
	for i := 0; i < input.Size()[0]; i++ {
		for j := 0; j < input.Size()[1]; j++ {
			index := i*input.Size()[1] + j
			l.biasGradients[j] += input.Vector()[index]
		}
	}
}

func (l *FullyConnectedLayer) calculateInputGradients(
	input, output *tensor.SimpleTensor,
) {
	weightMatrix := mat.NewDense(l.InputSize, l.OutputSize, l.weights)
	inputMatrix := mat.NewDense(input.Size()[0], l.OutputSize, input.Vector())
	output.Init(make([]float64, l.InputSize*input.Size()[0]),
		[]int{input.Size()[0], l.InputSize})
	outputMatrix := mat.NewDense(output.Size()[0], output.Size()[1],
		output.Vector())

	blas64.Gemm(blas.NoTrans, blas.Trans,
		1, inputMatrix.RawMatrix(), weightMatrix.RawMatrix(),
		1, outputMatrix.RawMatrix())
}

func (l *FullyConnectedLayer) saveForwardInput(input *tensor.SimpleTensor) {
	l.forwardInput = input.Vector()
}

func (l FullyConnectedLayer) Parameters() tensor.Vector {
	return tensor.CPUVector(l.parameters)
}

func (l FullyConnectedLayer) Gradients() tensor.Vector {
	return tensor.CPUVector(l.gradients)
}
