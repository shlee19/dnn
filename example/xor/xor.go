package main

import (
	"math/rand"

	"gitlab.com/akita/dnn/training/optimization"

	"gitlab.com/akita/dnn/layers"
	"gitlab.com/akita/dnn/training"
)

func main() {
	rand.Seed(1)

	network := training.Network{
		Layers: []layers.Layer{
			layers.NewFullyConnectedLayer(2, 2),
			layers.NewReluLayer(),
			layers.NewFullyConnectedLayer(2, 2),
			layers.NewSoftmaxLayer(2),
		},
	}
	trainer := training.Trainer{
		DataSource:      NewDataSource(),
		Network:         network,
		LossFunc:        training.CrossEntropy{},
		OptimizationAlg: optimization.NewSGD(0.03),
		Epoch:           2000,
		BatchSize:       4,
	}

	for _, l := range network.Layers {
		l.Randomize()
	}

	trainer.Train()
}
