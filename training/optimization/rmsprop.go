package optimization

import (
	"gitlab.com/akita/dnn/layers"
	"gitlab.com/akita/dnn/tensor"
)

// RMSProp runs RMSProp optimization algorithm.
type RMSProp struct {
	LearningRate float64
	SmoothFactor float64

	history map[layers.Layer]tensor.Vector
}

// NewRMSProp creates a new RMSProp optimization algorithm.
func NewRMSProp(learningRate float64) *RMSProp {
	return &RMSProp{
		LearningRate: learningRate,
		SmoothFactor: 0.9,
		history:      make(map[layers.Layer]tensor.Vector),
	}
}

func (r *RMSProp) UpdateParameters(layer layers.Layer) {
	params := layer.Parameters()
	gradients := layer.Gradients()

	if params == nil || gradients == nil {
		return
	}

	s, found := r.history[layer]
	if !found {
		s = gradients.Clone()
		s.MulElemWise(s)
		r.history[layer] = s
	} else {
		squaredGradients := gradients.Clone()
		squaredGradients.MulElemWise(squaredGradients)
		s.ScaleAdd(r.SmoothFactor, 1-r.SmoothFactor, squaredGradients)
	}

	temp := s.Clone()
	temp.PowerScalar(0.5)
	temp.AddScalar(1e-8)
	direction := gradients.Clone()
	direction.DivElemWise(temp)

	params.ScaleAdd(1, -1.0*r.LearningRate, direction)
}
