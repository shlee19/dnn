package optimization

import (
	"gitlab.com/akita/dnn/layers"
)

// SGD is an optimizer that runs SGD algorithm
type SGD struct {
	LearningRate float64
}

func NewSGD(learningRate float64) *SGD {
	sgd := &SGD{
		LearningRate: learningRate,
	}
	return sgd
}

func (s *SGD) UpdateParameters(layer layers.Layer) {
	params := layer.Parameters()
	gradients := layer.Gradients()

	if params == nil || gradients == nil {
		return
	}

	params.ScaleAdd(1, -1.0*s.LearningRate, gradients)
}
