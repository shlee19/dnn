package training

import (
	"math"

	"gitlab.com/akita/dnn/tensor"
)

// CrossEntropy calculates cross entropy loss function.
type CrossEntropy struct {
}

func (c CrossEntropy) Loss(
	output tensor.Tensor,
	label []int,
) (
	loss float64,
	derivative *tensor.SimpleTensor,
) {
	outputVector := output.Vector()
	loss = 0
	for i := 0; i < len(label); i++ {
		start := i * output.Size()[1]
		end := start + output.Size()[1]
		outputSlice := outputVector[start:end]
		loss += -1 * math.Log(outputSlice[label[i]])
	}
	loss /= float64(output.Size()[0])

	derivativeVector := make([]float64, len(outputVector))
	for i := 0; i < len(label); i++ {
		for j := 0; j < output.Size()[1]; j++ {
			index := i*output.Size()[1] + j
			if label[i] == j {
				derivativeVector[index] = -1 / outputVector[index]
			}
		}
	}

	derivative = &tensor.SimpleTensor{}
	derivative.Init(derivativeVector, output.Size())

	return loss, derivative
}
