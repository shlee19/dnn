package training

import (
	"log"

	"gitlab.com/akita/dnn/tensor"
	"gitlab.com/akita/dnn/training/optimization"
	"gitlab.com/akita/mgpusim/benchmarks/dnn/CCL"
	"gitlab.com/akita/mgpusim/driver"
)

type multiTrainer struct {
	driver  *driver.Driver
	context *driver.Context

	multiNetwork    []Network
	DataSource      DataSource
	LossFunc        LossFunction
	OptimizationAlg optimization.Alg
	Tester          *Tester
	Epoch           int
	BatchSize       int
	ShowBatchInfo   bool
	gpus            int

	communicator    CCL.Communicator
	exchangeBuffers []driver.GPUPtr
	recvBuffers     []driver.GPUPtr
	datalength      uint64
	chunk           uint64
}

func (t multiTrainer) multiTrain() {
	t.allocateExchangeBuffer()
	t.communicator = CCL.Communicator{
		Driver:      t.driver,
		Context:     t.context,
		SendBuffers: t.exchangeBuffers,
		RecvBuffers: t.recvBuffers,
		//Datalength:      t.datalength,
		//Chunk:           t.chunk,
		GPUs: 4,
	}

	for currentEpoch := 0; currentEpoch < t.Epoch; currentEpoch++ {
		log.Printf("Epoch %d\n", currentEpoch)
		t.DataSource.Rewind()
		batchNum := 0
		for {
			if t.ShowBatchInfo {
				for i := 0; i < 4; i++ {
					log.Printf("Batch %d\n", batchNum+i)
				}
			}
			var data [4]tensor.Tensor
			var label [4][]int
			for i := 0; i < t.gpus; i++ {
				data[i], label[i] = t.DataSource.NextBatch(t.BatchSize)
				if len(label) == 0 {
					break
				}
			}
			output := t.multiForward(data)
			derivative := t.multiCalculateLoss(output, label)
			t.multiBackward(derivative)
			////////////sync GPUs//////////////////////////
			t.memcpyToExchangeBuffer()
			t.allReduce()
			t.memcpyFromExchangeBuffer()
			///////////sync GPUs///////////////////////////
			t.multiUpdateParameters()
			batchNum = batchNum + 4
		}
		//t.test()
	}
}

func (t multiTrainer) allocateExchangeBuffer() {
	exchangeBuffers := make([]driver.GPUPtr, 4)
	recvBuffers := make([]driver.GPUPtr, 4)
	allgradients := 0
	bufferSize := 1024 * 1024 * 4

	//layersInNetworks := len(t.multiNetwork[0].Layers)
	//for i := 0; i < layersInNetworks; i++ {
	//	allgradient += t.multiNetwork[0].Layers[i].numGradients()
	//}

	for i := 0; i < t.gpus; i++ {
		t.driver.SelectGPU(t.context, i)
		exchangeBuffers[i-1] = t.driver.AllocateMemory(t.context, uint64(allgradients*4))
		recvBuffers[i-1] = t.driver.AllocateMemory(t.context, uint64(bufferSize))
	}
	t.exchangeBuffers = exchangeBuffers
	t.recvBuffers = recvBuffers
	t.datalength = uint64(allgradients)
	t.chunk = uint64(allgradients / 4)
}

func (t multiTrainer) memcpyToExchangeBuffer() {
	//t.driver.Remap()
}

func (t multiTrainer) allReduce() {
	t.communicator.AllReduce()
}

func (t multiTrainer) memcpyFromExchangeBuffer() {
	//t.driver.Remap()
	//Average//
}

func (t multiTrainer) multiForward(data [4]tensor.Tensor) [4]tensor.Tensor {
	var input [4]tensor.Tensor
	var output [4]tensor.Tensor
	output = data
	layersInNetworks := len(t.multiNetwork[0].Layers)
	for i := 0; i < layersInNetworks; i++ {
		for j := 0; j < t.gpus; j++ {
			input[j] = output[j]
			output[j] = t.multiNetwork[j].Layers[i].Forward(input[j])
		}
	}
	return output
}

func (t multiTrainer) multiCalculateLoss(output [4]tensor.Tensor, inputLabel [4][]int) [4]*tensor.SimpleTensor {
	var loss [4]float64
	var derivative [4]*tensor.SimpleTensor

	for i := 0; i < len(t.multiNetwork); i++ {
		loss[i], derivative[i] = t.LossFunc.Loss(output[i], inputLabel[i])
	}

	if t.ShowBatchInfo {
		for i := 0; i < len(t.multiNetwork); i++ {
			accuracy := calculateAccuracy(output[i], inputLabel[i])
			log.Printf("loss: %f, accuracy %f\n", loss, accuracy)
		}
	}
	return derivative
}

func (t multiTrainer) multiBackward(derivative [4]*tensor.SimpleTensor) {
	var output [4]tensor.Tensor
	var input [4]tensor.Tensor
	for i := 0; i < len(t.multiNetwork); i++ {
		output[i] = derivative[i]
	}
	layersInNetworks := len(t.multiNetwork[0].Layers)
	for i := layersInNetworks - 1; i >= 0; i-- {
		for j := 0; j < t.gpus; j++ {
			input[j] = output[j]
			output[j] = t.multiNetwork[j].Layers[i].Backward(input[j])
		}
	}
}

func (t multiTrainer) multiUpdateParameters() {
	layersInNetworks := len(t.multiNetwork[0].Layers)
	for i := 0; i < layersInNetworks; i++ {
		for j := 0; j < t.gpus; j++ {
			t.OptimizationAlg.UpdateParameters(t.multiNetwork[j].Layers[i])
		}
	}
}

func (t multiTrainer) test() {
	if t.Tester == nil {
		return
	}
	accuracy := t.Tester.Test()
	log.Printf("Accruacey %f\n", accuracy)
}
