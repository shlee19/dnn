// Package tensor defines the tensor interface.
package tensor

// A Tensor is a multi-dimension matrix.
type Tensor interface {
	// Init sets the data and dimensions of a tensor.
	Init(data []float64, size []int)

	// Size returns the length of the tensor in each dimension, from the
	// outermost dimension to the innermost dimension.
	Size() []int

	// Vector returns the data of the tensor represented in a pure vector.
	// Here, we use float64. However, the concrete tensor implementation can
	// use lower-precision numbers.
	Vector() []float64

	// Descriptor represents what does each dimension of the tensor represents.
	//
	// Tokens that represent the meaning of the dimensions include N, C, H, W.
	Descriptor() string
}

// A SimpleTensor is a multi-dimensional matrix.
type SimpleTensor struct {
	size       []int
	data       []float64
	descriptor string
}

func (t *SimpleTensor) Init(data []float64, size []int) {
	t.size = size
	t.data = data
}

func (t SimpleTensor) Size() []int {
	return t.size
}

func (t SimpleTensor) Vector() []float64 {
	return t.data
}

func (t SimpleTensor) Descriptor() string {
	return t.descriptor
}
