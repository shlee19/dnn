module gitlab.com/akita/dnn

go 1.13

require (
	github.com/golang/mock v1.4.4
	github.com/onsi/ginkgo v1.14.1
	github.com/onsi/gomega v1.10.2
	gitlab.com/akita/mgpusim v1.9.0
	gonum.org/v1/gonum v0.6.2
)

replace gitlab.com/akita/mgpusim => ../mgpusim
